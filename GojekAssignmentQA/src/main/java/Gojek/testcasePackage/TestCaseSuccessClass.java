package Gojek.testcasePackage;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import Gojek.FrameworkPackage.BrowserFactory;
import Gojek.UiPackage.BuyNowPage;
import Gojek.UiPackage.CheckoutPage;
import Gojek.UiPackage.CreditCardPage;
import Gojek.UiPackage.IssuingBankPage;
import Gojek.UiPackage.OrderSummaryPage;
import Gojek.UiPackage.SelectPaymentPage;

public class TestCaseSuccessClass extends BrowserFactory {
	
	 Logger logger= Logger.getLogger(TestCaseSuccessClass.class);
	
	public void pillowBooking() throws IOException, InterruptedException, NullPointerException
	{
		

		PageFactory.initElements(driver, BuyNowPage.class);
		logger.info("clicked on Buy now Button");
		BuyNowPage.Buynow.click();
		
		//System.out.println("clicked on buy now button");
		
		Thread.sleep(2000);
		
		PageFactory.initElements(driver, CheckoutPage.class);
		logger.info("clicked on Checkout Button");
		CheckoutPage.checkout.click();
		
		System.out.println("clicked on checkout button");
		Thread.sleep(2000);
		
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@id='snap-midtrans']")));
		
		Thread.sleep(2000);
		PageFactory.initElements(driver, OrderSummaryPage.class);
		logger.info("clicked on Continue Button");
		System.out.println("clicked on continue button");
		OrderSummaryPage.Countinue.click();
		

		Thread.sleep(2000);
	
		
		PageFactory.initElements(driver, SelectPaymentPage.class);
		logger.info("clicked on CreditCard Button");
		SelectPaymentPage.CreditCard.click();
		
		System.out.println("clicked on credit card");
		Thread.sleep(2000);
	}
	
	public TestCaseSuccessClass() {
		
	}
	@Test(priority=0)
	public void PillowBookingSuccess() throws InterruptedException, IOException {
		
		pillowBooking();
		
		
		PageFactory.initElements(driver, CreditCardPage.class);
		logger.info("Entering Credit Card Details");
		CreditCardPage.CardNumber.sendKeys(properties.getProperty("CreditCardNumber"));
		Thread.sleep(2000);
		CreditCardPage.ExpireDate.sendKeys(properties.getProperty("CreditCardExpiryDate"));
		Thread.sleep(2000);
		CreditCardPage.CVV.sendKeys(properties.getProperty("CreditCardCVV"));
		Thread.sleep(2000);
		logger.info("clicked on PayNow Button");
		CreditCardPage.PayNow.click();
		Thread.sleep(4000);
		
		driver.switchTo().parentFrame();
		
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@id='snap-midtrans']")));
		
		
		//driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@id='snap-midtrans']")));
		 driver.switchTo().frame(driver.findElement(By.xpath("//*[@id=\"application\"]/div[3]/div/div/div/iframe")));
		
		PageFactory.initElements(driver, IssuingBankPage.class);
		logger.info("Entering BankOTP");
		IssuingBankPage.Password.sendKeys(properties.getProperty("BankOTP"));
		Thread.sleep(2000);
		logger.info("clicked on OK Button");
		IssuingBankPage.OKbutton.click();
		Thread.sleep(2000);
		logger.info("Transaction successful");
	}
	
	/*@Test(priority=1)
	public void PillowBookingFailed() throws InterruptedException, IOException {
		
		pillowBooking();
		
		
		PageFactory.initElements(driver, CreditCardPage.class);
		CreditCardPage.CardNumber.sendKeys(properties.getProperty("CreditCardNumber2"));
		CreditCardPage.ExpireDate.sendKeys(properties.getProperty("CreditCardExpiryDate2"));
		CreditCardPage.CVV.sendKeys(properties.getProperty("CreditCardCVV2"));
		CreditCardPage.PayNow.click();
		
		
		Thread.sleep(4000);
		
		driver.switchTo().parentFrame();
		
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@id='snap-midtrans']")));
		
		
		
		 driver.switchTo().frame(driver.findElement(By.xpath("//*[@id=\"application\"]/div[3]/div/div/div/iframe")));
		
		PageFactory.initElements(driver, IssuingBankPage.class);
		IssuingBankPage.Password.sendKeys(properties.getProperty("BankOTP2"));
		Thread.sleep(2000);
		IssuingBankPage.OKbutton.click();
		Thread.sleep(2000);
		
	}*/
	
	
}
