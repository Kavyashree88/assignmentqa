package Gojek.FrameworkPackage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BrowserFactory {
	public static Properties properties=null;
    public static WebDriver driver=null;
    
    Logger logger= Logger.getLogger(BrowserFactory.class);
	

	public Properties loadPropertyFile() throws IOException{
		
		FileInputStream fileInputStream=new FileInputStream("config.properties");
		properties=new Properties();
		properties.load(fileInputStream);
		return properties;
		
	}
	
	@BeforeSuite
	public void launchBrowser() throws IOException {
		PropertyConfigurator.configure("log4j.properties");
		logger.info("Pillow Booking Test Begins");
		logger.info("Loading property file");
		
		loadPropertyFile();
		
		String browserName=properties.getProperty("BrowserName");
		String url=properties.getProperty("Url");
		String driverLocation=properties.getProperty("DriverLocation");
		
		if(browserName.equalsIgnoreCase("chrome")) {
			logger.info("Launching Chrome");
			System.setProperty("webdriver.chrome.driver",driverLocation);
			driver=new ChromeDriver();
			
		}
		else if(browserName.equalsIgnoreCase("firefox")) {
			logger.info("Launching Firefox");
			
		System.setProperty("webdriver.gecko.driver", driverLocation);
		driver=new FirefoxDriver();
		}
		driver.manage().window().maximize();
		
		logger.info("Navigating to Applcation");
		
		driver.get(url);
		//driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			
	}
	
	@AfterSuite
	public void teardown() {
		logger.info("Execution done. Closing the browser");
		
		driver.quit();
	}

}
